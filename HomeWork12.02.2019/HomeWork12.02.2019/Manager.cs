﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork12._02._2019
{
    public static class Manager
    {
        public static List<Student> CreateStudents(int count)
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            string[] mail = { "mail.ru", "gmail.com", "yandex.ru", "yahoo.com" };
            var students = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                var student = new Student
                {
                    Name = $"S{i + 1}",
                    SurName = $"S{i + 1}yan",
                    Age = (byte)rand.Next(18, 50),
                    Rayting = (byte)rand.Next(0, 100),
                    university = (University)rand.Next(0, 3),
                    email = mail[rand.Next(mail.Length)],
                };
                students.Add(student);
            }
            return students;
        }
        public static void PrintStudents(List<Student> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"{item.FullName}\tAge={item.Age}\tRayting={item.Rayting}\tEmail={item.email}\t\tUinversity={item.university}");
            }
        }
        public static List<Student> OrderByRayting(List<Student> list)
        {
            var listByRayting = list.OrderBy(p => p.Rayting).ToList();

            return listByRayting;
        }
        public static Dictionary<University, List<Student>> GroupByUniversity(List<Student> list)
        {
            //var groupByUniversity = from listst in list
            //                        group listst by listst.university into g
            //                        select 

            //return groupByUniversity;


            var groupByUniversity = list.GroupBy(p => p.university)
                                        .ToDictionary(g => g.Key, g => g.ToList());

            return groupByUniversity;
        }
        public static void PrintStudents(Dictionary<University, List<Student>> dic)
        {
            foreach (var item in dic)
            {
                Console.WriteLine(item.Key);
                PrintStudents(item.Value);
            }
        }

    }
}

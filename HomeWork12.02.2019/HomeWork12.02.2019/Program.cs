﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork12._02._2019
{
    class Program
    {
        static void Main(string[] args)
        {
            var students= Manager.CreateStudents(50);
            Manager.PrintStudents(students);
            Console.WriteLine();
            var orderByRaytingList=Manager.OrderByRayting(students);
            Console.WriteLine("Order By Rayting");
            Manager.PrintStudents(orderByRaytingList);
            Console.WriteLine();
            var groupByUniversity = Manager.GroupByUniversity(students);
            Console.WriteLine("Group By University");
            Manager.PrintStudents(groupByUniversity);
            Console.ReadLine();
        }
    }
}

﻿namespace HomeWork12._02._2019
{
    public class Student
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        public string FullName { get => $"{Name}\t{SurName}"; }
        public byte Age { get; set; }
        public byte Rayting { get; set; }
        public University university;
        public string email;
        public override string ToString()
        {
            return FullName;
        }
    }

    public enum University { ASUE,SEUA,YSMU,YSU }
}
